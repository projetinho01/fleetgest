﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class Aluguel
{
     [Key]
     [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
     public int Id { get; set; }
     public Veiculo Veiculo { get; set; }
     public Cliente Cliente { get; set; }
     public int Valor { get; set; }
     public DateTimeOffset DataInicio { get; set; }
     public DateTimeOffset DataFim { get; set; }

     public Aluguel()
     {
     }

     public Aluguel(int id, Veiculo veiculo, Cliente cliente, int valor, DateTimeOffset dataInicio, DateTimeOffset dataFim)
     {
          Id = id;
          Veiculo = veiculo;
          Cliente = cliente;
          Valor = valor;
          DataInicio = dataInicio;
          DataFim = dataFim;
     }
}