﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FleetGest.Domain.Enums;

namespace FleetGest.Domain;

public class Pagamento
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public Aluguel Aluguel { get; set; }
    public Veiculo Veiculo { get; set; }
    public Cliente Cliente { get; set; }
    public int Valor { get; set; }
    public DateTimeOffset Data { get; set; }
    public MetodoPagamento Metodo { get; set; }
    public StatusPagamento StatusPagamento { get; set; }

    public Pagamento()
    {
        
    }
    public Pagamento(int id, Aluguel aluguel, Veiculo veiculo, Cliente cliente, int valor, DateTimeOffset data, MetodoPagamento metodo, StatusPagamento statusPagamento)
    {
        Id = id;
        Aluguel = aluguel;
        Veiculo = veiculo;
        Cliente = cliente;
        Valor = valor;
        Data = data;
        Metodo = metodo;
        StatusPagamento = statusPagamento;
    }
}