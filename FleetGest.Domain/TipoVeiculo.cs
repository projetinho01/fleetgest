﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class TipoVeiculo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Nome { get; set; }
    public string Observacao { get; set; }

    public TipoVeiculo()
    {
        
    }
    public TipoVeiculo(int id, string nome, string observacao)
    {
        Id = id;
        Nome = nome;
        Observacao = observacao;
    }
}