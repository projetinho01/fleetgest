﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FleetGest.Domain.Enums;

namespace FleetGest.Domain;

public class Manutencao
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public Oficina Oficina { get; set; }
    public DateTimeOffset Data { get; set; }
    public TipoManutencao Tipo { get; set; }
    public string AcoesRealizadas { get; set; }
    public string Motivo { get; set; }
    public int ValorPeca { get; set; }
    public int ValorMaoDeObra { get; set; }
    public int KmAtual { get; set; }

    public Manutencao()
    {
        
    }
    public Manutencao(int id, Oficina oficina, DateTimeOffset data, TipoManutencao tipo, string acoesRealizadas, string motivo, int valorPeca, int valorMaoDeObra, int kmAtual)
    {
        Id = id;
        Oficina = oficina;
        Data = data;
        Tipo = tipo;
        AcoesRealizadas = acoesRealizadas;
        Motivo = motivo;
        ValorPeca = valorPeca;
        ValorMaoDeObra = valorMaoDeObra;
        KmAtual = kmAtual;
    }
}