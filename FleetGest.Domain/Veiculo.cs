﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class Veiculo
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public TipoVeiculo TipoVeiculo { get; set; }
    public string Modelo { get; set; }
    public string Placa { get; set; }
    public string Renavam { get; set; }
    public string Chassi { get; set; }
    public int RodagemInicial { get; set; }
    public int RodagemAtual { get; set; }
    public string Cor { get; set; }
    public int Ano { get; set; }
    public int PagamentoIpva { get; set; }
    public int PagamentoSeguro { get; set; }
    public Propietario Propietario { get; set; }

    public Veiculo()
    {
        
    }
    public Veiculo(int id, TipoVeiculo tipoVeiculo, string modelo, string placa, string renavam, string chassi, int rodagemInicial, int rodagemAtual, string cor, int ano, int pagamentoIpva, int pagamentoSeguro, Propietario propietario)
    {
        Id = id;
        TipoVeiculo = tipoVeiculo;
        Modelo = modelo;
        Placa = placa;
        Renavam = renavam;
        Chassi = chassi;
        RodagemInicial = rodagemInicial;
        RodagemAtual = rodagemAtual;
        Cor = cor;
        Ano = ano;
        PagamentoIpva = pagamentoIpva;
        PagamentoSeguro = pagamentoSeguro;
        Propietario = propietario;
    }
}