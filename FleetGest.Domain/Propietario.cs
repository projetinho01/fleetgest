﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class Propietario
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Endereco { get; set; }
    public string Nome { get; set; }
    public string Telefone { get; set; }
    public string Cpf { get; set; }

    public Propietario()
    {
        
    }
    public Propietario(int id = default, string endereco = null, string nome = null, string telefone = null, string cpf = null)
    {
        Id = id;
        Endereco = endereco;
        Nome = nome;
        Telefone = telefone;
        Cpf = cpf;
    }
}