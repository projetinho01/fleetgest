﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class Oficina
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Nome { get; set; }
    public string Endereco { get; set; }
    public string Telefone { get; set; }
    public string NomeContato { get; set; }

    public Oficina()
    {
        
    }
    public Oficina(int id, string nome, string endereco, string telefone, string nomeContato)
    {
        Id = id;
        Nome = nome;
        Endereco = endereco;
        Telefone = telefone;
        NomeContato = nomeContato;
    }
}