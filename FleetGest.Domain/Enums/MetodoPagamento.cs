﻿namespace FleetGest.Domain.Enums;

public enum MetodoPagamento
{
    Cartao,
    Dinheiro,
    Pix
}