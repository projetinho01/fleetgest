﻿namespace FleetGest.Domain.Enums;

public enum TipoManutencao
{
    Pneu,
    Oleo,
    Pintura,
    Farol
}