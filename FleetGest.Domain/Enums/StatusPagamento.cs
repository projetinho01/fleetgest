﻿namespace FleetGest.Domain.Enums;

public enum StatusPagamento
{
    Aprovado,
    Negado,
    Analise
}