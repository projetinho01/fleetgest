﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FleetGest.Domain;

public class Cliente
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Nome { get; set; }
    public string Endereco { get; set; }
    public string Telefone { get; set; }
    public string Cpf { get; set; }

    public Cliente()
    {
    }

    public Cliente(int id, string nome, string endereco, string telefone, string cpf)
    {
        Id = id;
        Nome = nome;
        Endereco = endereco;
        Telefone = telefone;
        Cpf = cpf;
    }
}