﻿using FleetGest.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FleetGest.Infra;

public class ApplicationDbContext : DbContext
{

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public DbSet<Aluguel> Aluguel { get; set; }
    public DbSet<Cliente> Clientes { get; set; }
    public DbSet<Manutencao> Manutencoess { get; set; }
    public DbSet<Oficina> Oficinas { get; set; }
    public DbSet<Pagamento> Pagamentos { get; set; }
    public DbSet<Propietario> Propietarios { get; set; }
    public DbSet<TipoVeiculo> TipoVeiculos { get; set; }
    public DbSet<Veiculo> Veiculos { get; set; }
}